#!/usr/bin/env python3

import turtle


state = {'angle': 0}


def spinner():

    turtle.clear()
    angle = state['angle'] / 10
    turtle.right(angle)

    dot_colors = ('red', 'green', 'blue')
    for dot_color in dot_colors:
        turtle.forward(100)
        turtle.dot(120, dot_color)
        turtle.back(100)
        turtle.right(120)

    turtle.update()


def animate():
    if state['angle'] > 0:
        state['angle'] -= 1
    spinner()
    turtle.ontimer(animate, 20)


def flick():
    state['angle'] += 10


if __name__ == '__main__':
    turtle.setup(420, 420, 370, 0)
    turtle.hideturtle()
    turtle.tracer(False)
    turtle.width(20)
    turtle.onkey(flick, 'space')
    turtle.listen()
    animate()
    turtle.done()
