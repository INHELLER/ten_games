#!/usr/bin/env python3

import random
import turtle

import base


def rand_value(rand_sequences=((-5, -3), (3, 5))):
    min_val, max_val = random.choice(rand_sequences)
    return (random.random() * (max_val - min_val) + min_val)


def draw():
    ball.move(aim)

    if ball.x < -200 or ball.x > 200:
        aim.x = -aim.x
    if ball.y < -200 or ball.y > 200:
        aim.y = -aim.y

    turtle.clear()
    turtle.goto(*ball.get_as_tuple())
    turtle.dot(10, 'blue')
    turtle.ontimer(draw, 50)


ball = base.Vector(0, 0)
aim = base.Vector(rand_value(), rand_value())


if __name__ == '__main__':
    turtle.setup(420, 420, 370, 0)
    turtle.hideturtle()
    turtle.tracer(False)
    turtle.up()
    draw()
    turtle.done()
