#!/usr/bin/env python3

import random
import turtle

import base


def rand_value(rand_sequences=((-5, -3), (3, 5))):
    min_val, max_val = random.choice(rand_sequences)
    return (random.random() * (max_val - min_val) + min_val)


ball = base.Vector(0, 0)
aim = base.Vector(rand_value(), rand_value())

state = {1: 0, 2: 0}


def move(player, change):
    state[player] += change


def rectangle(x, y, width, height):
    turtle.up()
    turtle.goto(x, y)
    turtle.down()
    turtle.begin_fill()

    for count in range(2):
        turtle.forward(width)
        turtle.left(90)
        turtle.forward(height)
        turtle.left(90)
    turtle.end_fill()


def draw():
    turtle.clear()
    rectangle(-200, state[1], 10, 50)
    rectangle(190, state[2], 10, 50)

    ball.move(aim)
    x = ball.x
    y = ball.y
    turtle.up()
    turtle.goto(x, y)
    turtle.dot(10)
    turtle.update()

    if y < -200 or y > 200:
        aim.y = -aim.y

    if x < -185:
        low = state[1]
        high = state[1] + 50
        if low <= y <= high:
            aim.x = -aim.x
        else:
            return

    if x > 185:
        low = state[1]
        high = state[2] + 50
        if low <= y <= high:
            aim.x = -aim.x
        else:
            return

    turtle.ontimer(draw, 50)


if __name__ == '__main__':
    turtle.setup(420, 420, 370, 0)
    turtle.hideturtle()
    turtle.tracer(False)
    turtle.listen()

    turtle.onkey(lambda : move(1, 20), 'w')
    turtle.onkey(lambda : move(1, -20), 's')
    turtle.onkey(lambda : move(2, 20), 'i')
    turtle.onkey(lambda : move(2, -20), 'k')

    turtle.onkey(draw, 'space')

    turtle.done()
