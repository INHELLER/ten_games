#!/usr/bin/env python3

import random
import turtle

import base


def draw():
    turtle.color('black')
    turtle.width(5)

    for x in range(-200, 200, 20):
        for y in range(-200, 200, 20):
            if random.random() > 0.5:
                base.line(x, y, x + 20, y + 20)
            else:
                base.line(x, y + 20, x + 20, y)

    turtle.update()


def tap(x, y):
    if abs(x) > 198 or abs(y) > 198:
        turtle.up()
    else:
        turtle.down()

    turtle.width(2)
    turtle.color('red')
    turtle.goto(x, y)
    turtle.dot(4)


if __name__ == '__main__':
    turtle.setup(420, 420, 370, 0)
    turtle.hideturtle()
    turtle.tracer(False)

    draw()

    turtle.onscreenclick(tap)
    turtle.done()
