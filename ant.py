#!/usr/bin/env python3

import random
import turtle
import base


def wrap(value):
    return value


def init():
    turtle.setup(480, 480)


def draw():
    ant.move(aim)
    ant.x = wrap(ant.x)
    ant.y = wrap(ant.y)
    aim.move(random.random() - 0.5)

    aim.rotate(random.random() * 10 - 5)
    turtle.clear()
    turtle.goto(ant.x, ant.y)
    turtle.dot(4)

    if running:
        turtle.ontimer(draw, 100)



ant = base.Vector(0, 0)
aim = base.Vector(2, 0)

if __name__ == '__main__':
    init()
    turtle.hideturtle()
    turtle.tracer(False)
    turtle.up()
    running = True
    draw()
    turtle.done()
