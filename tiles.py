#!/usr/bin/env python3

import random
import turtle
import base


tiles = {}

neighbors = [
    base.Vector(100, 0),
    base.Vector(-100, 0),
    base.Vector(0, 100),
    base.Vector(0, -100),
]


def load():
    count = 1
    mark = None
    for y in range(-200, 200, 100):
        for x in range(-200, 200, 100):
            mark = base.Vector(x, y)
            tiles[mark] = count
            count += 1

    tiles[mark] = None

    for _ in range(random.randint(900, 1300)):
        neighbor = random.choice(neighbors)
        spot = mark + neighbor

        if spot in tiles:
            number = tiles[spot]
            tiles[spot] = None
            tiles[mark] = number
            mark = spot


def square(mark, number):
    turtle.up()
    turtle.goto(*mark.get_as_tuple())
    turtle.down()
    turtle.color('black', 'white')
    turtle.begin_fill()

    for _ in range(4):
        turtle.forward(99)
        turtle.left(90)

    turtle.end_fill()

    if number is None:
        return
    elif number < 10:
        turtle.forward(30)

    turtle.write(number, font=('Arial', 60, 'normal'))


def draw():
    for mark in tiles:
        square(mark, tiles[mark])
        turtle.update()


def tap(x, y):
    x = base.floor(x, 100)
    y = base.floor(y, 100)

    mark = base.Vector(x, y)

    for neighbor in neighbors:
        spot = mark + neighbor
        if spot in tiles and tiles[spot] is None:
            number = tiles[mark]
            tiles[mark] = None
            tiles[spot] = number
            square(spot, number)
            square(mark, None)


if __name__ == '__main__':
    turtle.setup(420, 420, 370, 0)
    turtle.hideturtle()
    turtle.tracer(False)
    turtle.listen()

    load()
    draw()

    turtle.onscreenclick(tap)

    turtle.done()
