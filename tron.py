#!/usr/bin/env python3

import turtle
import base


p1xy = base.Vector(-100, 0)
p1aim = base.Vector(4, 0)
p1body = set()

p2xy = base.Vector(100, 0)
p2aim = base.Vector(-4, 0)
p2body = set()


def inside(head):
    return -200 < head.x < 200 and -200 < head.y < 200


def draw():
    p1xy.move(p1aim)
    p1head = p1xy.copy()

    p2xy.move(p2aim)
    p2head = p2xy.copy()

    if not inside(p1head) or p1head in p2body:
        print('Player blue has won!')
        return

    if not inside(p2head) or p2head in p1body:
        print('Player red has won!')
        return

    p1body.add(p1head)
    p2body.add(p2head)

    base.square(*p1xy.get_as_tuple(), 3, 'red')
    base.square(*p2xy.get_as_tuple(), 3, 'blue')
    turtle.update()
    turtle.ontimer(draw, 50)


if __name__ == '__main__':
    turtle.setup(420, 420, 370, 0)
    turtle.hideturtle()
    turtle.tracer(False)
    turtle.listen()

    turtle.onkey(lambda : p1aim.rotate(90), 'a')
    turtle.onkey(lambda: p1aim.rotate(-90), 'd')

    turtle.onkey(lambda: p2aim.rotate(90), 'j')
    turtle.onkey(lambda: p2aim.rotate(-90), 'l')

    draw()

    turtle.done()
