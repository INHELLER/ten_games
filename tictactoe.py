#!/usr/bin/env python3

import turtle
import base


def grid():
    for i in range(-60, 61, 120):
        base.line(-180, i, 180, i)
        base.line(i, -180, i, 180)
    turtle.update()


def draw_x(x, y):
    base.line(x + 10, y + 10, x + 110, y + 110)
    base.line(x + 110, y + 10, x + 10, y + 110)
    turtle.update()


def draw_o(x, y):
    turtle.up()
    turtle.goto(x + 60, y + 10)
    turtle.down()
    turtle.circle(50)
    turtle.update()


def floor(value):
    return ((value + 60) // 120) * 120 - 60


def tap(x, y):
    x = floor(x)
    y = floor(y)
    player = state['player']
    players[player](x, y)
    state['player'] = not state['player']


state = {'player': 0}
players = [draw_x, draw_o]


if __name__ == '__main__':
    turtle.setup(420, 420, 370, 0)
    turtle.hideturtle()
    turtle.tracer(False)

    grid()
    turtle.onscreenclick(tap)

    turtle.done()
