#!/usr/bin/env python3

import turtle
import random

import base


class Snake:

    def __init__(self):
        self.head = base.Vector(0, 0)
        self.body = [base.Vector(0, -20), base.Vector(0, -10)]

        self.head_color = 'red'

        self.direction = base.Vector(0, 10)
        self.apple = None

        self.generate_apple()

    def generate_apple(self):
        apple = base.Vector(random.randrange(-15, 15) * 10, random.randrange(-15, 15) * 10)
        while apple in self.body or apple == self.head:
            apple = base.Vector(random.randrange(-19, 19) * 10, random.randrange(-19, 19) * 10)
        self.apple = apple

    def change_direction(self, x, y):
        invalid_direction = self.body[-1] - self.head
        new_direction = base.Vector(x, y)
        if new_direction != invalid_direction:
            self.direction = new_direction

    def is_collided(self):
        in_bounds = -190 < self.head.x < 190 and -190 < self.head.y < 190
        self_collided = self.head in self.body
        return (not in_bounds) or self_collided

    def ate_apple(self):
        return self.head == self.apple

    def move(self):
        new_head = self.head.copy()
        new_head.move(self.direction)
        if self.is_collided():
            self.head_color = 'blue'
            return False

        self.body.append(self.head)
        self.head = new_head
        if self.ate_apple():
            self.generate_apple()
        else:
            self.body.pop(0)

        return True

    def draw(self):
        turtle.clear()

        for segment in self.body:
            base.square(*segment.get_as_tuple(), 10, 'black')

        base.square(*self.head.get_as_tuple(), 10, self.head_color)

        base.square(*self.apple.get_as_tuple(), 10, 'green')

        draw_grid()

        turtle.update()

    def game_loop(self):
        status = self.move()
        self.draw()
        if not status:
            return
        turtle.ontimer(self.game_loop, 250)

    def run(self):
        turtle.setup(420, 420, 370, 0)
        turtle.hideturtle()
        turtle.tracer(False)
        turtle.listen()

        turtle.onkey(lambda : self.change_direction(-10, 0), 'Left')
        turtle.onkey(lambda : self.change_direction(10, 0), 'Right')
        turtle.onkey(lambda : self.change_direction(0, 10), 'Up')
        turtle.onkey(lambda : self.change_direction(0, -10), 'Down')

        self.game_loop()

        turtle.done()


def draw_grid():
    turtle.color('green')
    for i in range(-200, 201, 10):
        base.line(-200, i, 200, i)
        base.line(i, -200, i, 200)


if __name__ == '__main__':
    game = Snake()
    game.run()
