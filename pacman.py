#!/usr/bin/env python3

import random
import turtle

import base


state = {'score': 0}
path = turtle.Turtle(visible=False)
writer = turtle.Turtle(visible=False)

pacman = base.Vector(-40, -80)
aim = base.Vector(5, 0)

ghosts = [
    [base.Vector(-180, 160), base.Vector(5, 0)],
    [base.Vector(-180, 160), base.Vector(0, 5)],
    [base.Vector(100, 160), base.Vector(0, -5)],
    [base.Vector(100, -160), base.Vector(-5, 0)],
]

tiles = [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
    0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
    0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0,
    0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0,
    0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
    0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0,
    0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
    0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
    0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0,
    0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0,
    0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0,
    0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
    0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
]


def square(x, y):
    path.up()
    path.goto(x, y)
    path.down()
    path.begin_fill()

    for _ in range(4):
        path.forward(20)
        path.left(90)

    path.end_fill()


def offset(point):
    x = (base.floor(point.x, 20) + 200) / 20
    y = (180 - base.floor(point.y, 20)) / 20

    index = int(x + y * 20)

    return index


def valid(point):
    index = offset(point)

    if tiles[index] == 0:
        return False
    index = offset(point + 19)
    if tiles[index] == 0:
        return False

    return point.x % 20 == 0 or point.y % 20 == 0


def world():
    turtle.bgcolor('black')
    path.color('blue')
    for index, tile in enumerate(tiles):
        if tile > 0:
            x = (index % 20) * 20 - 200
            y = 180 - (index // 20) * 20
            square(x, y)
            if tile == 1:
                path.up()
                path.goto(x+10, y+10)
                path.dot(2, 'white')


def move():
    writer.undo()
    writer.write(state['score'])
    turtle.clear()
    if valid(pacman + aim):
        pacman.move(aim)

    index = offset(pacman)
    if tiles[index] == 1:
        tiles[index] = 2
        state['score'] += 1
        x = (index % 20) * 20 - 200
        y = 180 - (index // 20) * 20
        square(x, y)

    turtle.up()
    turtle.goto(pacman.x + 10, pacman.y + 10)
    turtle.dot(20, 'yellow')

    for point, course in ghosts:
        if valid(point + course):
            point.move(course)
        else:
            options = [
                base.Vector(5, 0),
                base.Vector(-5, 0),
                base.Vector(0, -5),
                base.Vector(0, 5),
            ]

            plan = random.choice(options)
            course.x = plan.x
            course.y = plan.y

        turtle.up()
        turtle.goto(point.x + 10, point.y + 10)
        turtle.dot(20, 'red')
    turtle.update()

    for point, _ in ghosts:
        if abs(pacman - point) < 20:
            return

    turtle.ontimer(move, 100)


def change(x, y):
    if valid(pacman + base.Vector(x, y)):
        aim.x = x
        aim.y = y


if __name__ == '__main__':
    turtle.setup(420, 420, 370, 0)
    turtle.hideturtle()
    turtle.tracer(False)
    writer.goto(160, 160)
    writer.color('white')
    writer.write(state['score'])
    turtle.listen()

    turtle.onkey(lambda : change(5, 0), 'Right')
    turtle.onkey(lambda: change(-5, 0), 'Left')
    turtle.onkey(lambda: change(0, 5), 'Up')
    turtle.onkey(lambda: change(0, -5), 'Down')

    world()
    move()

    turtle.done()
