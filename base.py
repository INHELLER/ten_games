#!/usr/bin/env python3

import turtle
import collections
import math
import os
import operator


def floor(value, size, offset=200):
    return float(((value + offset) // size) * size - offset)


def path(filename):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    return os.path.join(dir_path, filename)


def line(x_start, y_start, x_end, y_end):
    turtle.up()
    turtle.goto(x_start, y_start)
    turtle.down()
    turtle.goto(x_end, y_end)


def square(x, y, sq_size, sq_color='black'):
    turtle.up()
    turtle.goto(x, y)
    turtle.down()
    turtle.color(sq_color)

    turtle.begin_fill()

    for _ in range(4):
        turtle.forward(sq_size)
        turtle.left(90)

    turtle.end_fill()


class Vector(collections.Sequence):
    PRECISION = 6
    __slots__ = ('_x', '_y', '_hash')

    def __init__(self, x, y):
        self._hash = None
        self._x = round(x, self.PRECISION)
        self._y = round(y, self.PRECISION)

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        if self._hash is not None:
            raise ValueError('Cannot set x after hashing')
        self._x = round(value, self.PRECISION)

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        if self._hash is not None:
            raise ValueError('Cannot set y after hashing')
        self._y = round(value, self.PRECISION)

    def __eq__(self, other):
        cls = type(self)
        if isinstance(other, cls):
            return self.x == other.x and self.y == other.y
        return NotImplemented

    def __ne__(self, other):
        cls = type(self)
        if isinstance(other, cls):
            return self.x != other.x or self.y != other.y
        return NotImplemented

    def __iadd__(self, other):
        return self._make_operation(other, '+')

    def __add__(self, other):
        copy = self.copy()
        copy += other
        return copy

    __radd__ = __add__

    def __isub__(self, other):
        return self._make_operation(other, '-')

    def __sub__(self, other):
        copy = self.copy()
        copy -= other
        return copy

    __rsub__ = __sub__

    def __imul__(self, other):
        return self._make_operation(other, '*')

    def __mul__(self, other):
        copy = self.copy()
        copy *= other
        return copy

    __rmul__ = __mul__

    def __itruediv__(self, other):
        return self._make_operation(other, '/')

    def __truediv__(self, other):
        copy = self.copy()
        copy /= other
        return copy

    __rtruediv__ = __truediv__

    def __neg__(self):
        cls = type(self)
        return cls(-self.x, -self.y)

    def __abs__(self):
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def __hash__(self):
        if self._hash is None:
            pair = (self.x, self.y)
            self._hash = hash(pair)

        return self._hash

    def __len__(self):
        return 2

    def __getitem__(self, item):
        out_val = (self.x, self.y)
        return out_val[item]

    def __repr__(self):
        cls = type(self)
        return '{}({!r}, {!r})'.format(cls.__name__, self.x, self.y)

    def _make_operation(self, other, in_op):
        if self._hash is not None:
            raise ValueError('Cannot true divide vector after hashing.')

        operations = {
            '+': operator.add,
            '-': operator.sub,
            '*': operator.mul,
            '/': operator.truediv,
        }

        # iadd почему-то не работает. Поэтому таким образом.
        self.x = operations[in_op](self.x, getattr(other, 'x', other))
        self.y = operations[in_op](self.y, getattr(other, 'y', other))

        return self

    def copy(self):
        cls = type(self)
        return cls(self.x, self.y)

    def get_as_tuple(self):
        return self.x, self.y

    def move(self, other):
        self.__iadd__(other)

    def scale(self, other):
        self.__imul__(other)

    def rotate(self, angle):
        if self._hash is not None:
            raise ValueError('Cannot rotate vector after hashing.')
        x = self.x
        y = self.y
        radians = math.radians(angle)
        self.x = x * math.cos(radians) - y * math.sin(radians)
        self.y = y * math.cos(radians) + x * math.sin(radians)


if __name__ == '__main__':
    a = Vector(1, 2)
    b = Vector(3, 4)
    a += b
    print(a, b)
    c = a + b
    print(a, b, c)
    d = a * b
    print(a, b, c, d)
