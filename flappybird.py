#!/usr/bin/env python3

import random
import turtle

import base


bird = base.Vector(0, 0)
balls = []


def inside(point):
    return -200 < point.x < 200 and -200 < point.y < 200


def draw(alive):
    turtle.clear()
    turtle.goto(bird.x, bird.y)
    turtle.dot(10, 'green') if alive else turtle.dot(10, 'red')

    for ball in balls:
        turtle.goto(ball.x, ball.y)
        turtle.dot(20, 'black')

    turtle.update()


def move():
    bird.y -= 5
    for ball in balls:
        ball.x -= 3

    if random.randrange(10) == 0:
        y = random.randrange(-199, 199)
        ball = base.Vector(199, y)
        balls.append(ball)

    while len(balls) and not inside(balls[0]):
        balls.pop(0)

    if not inside(bird):
        draw(False)
        return

    for ball in balls:
        if abs(ball - bird) < 15:
            draw(False)
            return

    draw(True)
    turtle.ontimer(move, 50)


def tap(x, y):
    up = base.Vector(0, 30)
    bird.move(up)


if __name__ == '__main__':
    turtle.setup(420, 420, 370, 0)
    turtle.hideturtle()
    turtle.up()
    turtle.tracer(False)
    turtle.listen()

    turtle.onscreenclick(tap)
    turtle.onkey(lambda : tap(1, 2), 'space')
    move()

    turtle.done()
